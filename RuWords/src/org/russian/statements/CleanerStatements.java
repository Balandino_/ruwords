package org.russian.statements;


import java.util.Arrays;
import java.util.List;

public class CleanerStatements {

	
	public static final String[] verbHeaders = new String[] {"bare", "accented", "audio", "type", "aspect", "partner", "imperative_sg", "imperative_pl", "past_m", "past_f", "past_n", "past_pl", "sg1", "sg2", "sg3", "pl1", "pl2", "pl3", "tl"};
	public static final List<String> nonRussianVerbHeaders = Arrays.asList(new String[] {"audio", "type", "aspect", "tl"});
	
	public static final String  extractAllVerbs = "WITH allVerbs AS (\n"
			+ "    SELECT bare AS searchBare FROM words WHERE type = \"verb\"\n"
			+ ") SELECT bare, accented, audio, type, aspect, partner, imperative_sg, imperative_pl, past_m, past_f, past_n, past_pl, sg1, sg2, sg3, pl1, pl2, pl3, tl FROM words \n"
			+ "    INNER JOIN verbs ON words.id = verbs.word_id \n"
			+ "        INNER JOIN conjugations ON words.id = conjugations.word_id \n"
			+ "                INNER JOIN translations ON words.id = translations.word_id\n"
			+ "                    INNER JOIN allVerbs ON words.bare = allVerbs.searchBare\n"
			+ "                        WHERE lang = \"en\"";
	
	
	public static final String[] nounHeaders = new String[] { "bare", "accented", "audio", "type", "nom", "acc", "gen", "dat", "inst", "prep", "gender", "animate", "indeclinable", "sg_only", "pl_only", "tl"};
	public static final List<String> nonRussianNounHeaders = Arrays.asList(new String[] {"audio", "type", "gender", "animate", "indeclinable", "sg_only", "pl_only", "tl"});
	public static final String extractAllNouns = "WITH allNouns AS (\n"
			+ "     SELECT bare AS searchBare FROM words WHERE type = \"noun\"\n"
			+ ") SELECT bare, accented, audio, type, nom, acc, gen, dat, inst, prep, gender, animate, indeclinable, sg_only, pl_only, tl FROM words \n"
			+ "    INNER JOIN nouns ON words.id = nouns.word_id \n"
			+ "        INNER JOIN declensions ON words.id = declensions.word_id \n"
			+ "            INNER JOIN translations ON words.id = translations.word_id \n"
			+ "                INNER JOIN allNouns ON words.bare = allNouns.searchBare \n"
			+ "                    WHERE lang = \"en\"\n";
	
	
	public static final String[] adjectiveHeaders = new String[] {"bare", "accented", "audio", "type", "comparative", "superlative", "short_m", "short_f", "short_n", "short_pl", "nom", "acc", "gen", "dat", "inst", "prep", "tl"};
	public static final List<String> nonRussianAdjectiveHeaders = Arrays.asList(new String[] {"audio", "type", "tl"});
	public static final String extractAllAdjectives = "WITH allAdjectives AS (\n"
			+ "    SELECT bare AS searchBare FROM words WHERE type = \"adjective\"\n"
			+ ") SELECT bare, accented, audio, type, comparative, superlative, short_m, short_f, short_n, short_pl, nom, acc, gen, dat, inst, prep, tl FROM words \n"
			+ "    INNER JOIN adjectives ON words.id = adjectives.word_id \n"
			+ "        INNER JOIN declensions ON words.id = declensions.word_id \n"
			+ "            INNER JOIN translations ON words.id = translations.word_id\n"
			+ "                INNER JOIN allAdjectives ON words.bare = allAdjectives.searchBare \n"
			+ "                    WHERE lang = \"en\"\n";
	
	
	
	public static final String[] otherHeaders = new String[] {"bare", "accented", "audio", "type", "tl"};
	public static final List<String> nonRussianOtherHeaders = Arrays.asList(new String[] {"audio", "type", "tl"});
	public static final String extractAllOthers = "SELECT bare, accented, audio, type, tl FROM words \n"
			+ "    INNER JOIN translations ON words.id = translations.word_id \n"
			+ "        WHERE type = \"other\" AND lang = \"en\""; 
	
	
	
	
}

