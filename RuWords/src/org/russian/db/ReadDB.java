package org.russian.db;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.russian.statements.Statements;
import org.sqlite.SQLiteConfig;

public class ReadDB {
	public static void main(String[] args) {
		final long programStart = System.nanoTime();
		final boolean inMemory = false;
		final Path inputFile = Paths.get("/home/mkg/Desktop/WordData/TestWords");
		final String database = "./Modified Database/Russian_Words_Lite_Modified.sqlite3";
		
		SQLiteConfig config = new SQLiteConfig();
		config.setReadOnly((inMemory ? false : true));
		
		final String url = (inMemory ? "jdbc:sqlite::memory:" : ("jdbc:sqlite:" + database));

		try (Connection conn = DriverManager.getConnection(url, config.toProperties())) {
			try (Statement statement = conn.createStatement()) {
				if (inMemory) {
					statement.execute("restore from " + "'" + database +  "'");
				}
				System.out.println("Connection to " + (inMemory ? "in-memory " : "") +  "database has been established!");
				
				//Prepare Statements
				PreparedStatement preppedBareSearch = conn.prepareStatement(Statements.extractBare);
				PreparedStatement preppedNoun = conn.prepareStatement(Statements.extractNoun);
				PreparedStatement preppedAdjective = conn.prepareStatement(Statements.extractAdjective);
				PreparedStatement preppedOther = conn.prepareStatement(Statements.extractOther);
				PreparedStatement preppedVerbAndPartners = conn.prepareStatement(Statements.extractVerbAndPartners);
				
				//Obtain words to process
				long wordCount = 0;
				long matchedCount = 0;
				long totalStringSize = 0;
				
				String words = Files.readString(inputFile, StandardCharsets.UTF_8);
				Pattern pattern = Pattern.compile("([А-Яа-яЁё-]+[А-Яа-яЁё]+)|([А-Яа-яЁё]+)", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
				HashMap<String, String> definitions = new HashMap<String, String>();
				HashSet<String> unknownWords = new HashSet<String>();
				Matcher matcher = pattern.matcher(words);
				
				final long sqlStart = System.nanoTime();
				while (matcher.find()) {
					String searchWord = matcher.group().toLowerCase();
					wordCount++;

					if (definitions.containsKey(searchWord) || unknownWords.contains(searchWord)) {// Skip duplicates
						if (definitions.containsKey(searchWord)) {
							matchedCount++;
						}
						continue;
					}
					
					
					preppedBareSearch.setString(1, searchWord.toLowerCase());

					// Get bare word & type matches
					StringBuilder searchResult = new StringBuilder();
					ResultSet results = preppedBareSearch.executeQuery();
					while (results.next()) {
						searchResult.append(results.getString(1) + "," + results.getString(2));
						searchResult.append("|");
					}
					
					
					// Confirm matches are present
					if (searchResult.length() == 0) {
						//System.out.println("No match! -> " + searchWord);
						unknownWords.add(searchWord);
						continue;
					}

					// Process all matches
					StringBuilder extract = new StringBuilder("{" + "\"" + searchWord + "\": [");
					String[] wordsAndTypes = searchResult.toString().substring(0, searchResult.length() -1).split("\\|");
					
					
					boolean previousMatch = false; //Used to detect if multiple types (e.g noun & verb) are used in the loop
					for (String match : wordsAndTypes) {
						String[] wordAndType = match.split(",");
						switch (wordAndType[1]) {
						case "verb":
							ReadDB.editIfPreviousMatch(previousMatch, extract);//Edits string to allow new array for next word type
							extract.append(ReadDB.searchForWordEntry(preppedVerbAndPartners, 19, 1, 2, wordAndType[0]));
							previousMatch = true;
							break;
							
						case "noun":
							ReadDB.editIfPreviousMatch(previousMatch, extract);//Edits string to allow new array for next word type
							extract.append(ReadDB.searchForWordEntry(preppedNoun, 16, 4, 1, wordAndType[0]));
							previousMatch = true;
							break;

						case "adjective":
							ReadDB.editIfPreviousMatch(previousMatch, extract);//Edits string to allow new array for next word type
							extract.append(ReadDB.searchForWordEntry(preppedAdjective, 17, 4, 1, wordAndType[0]));
							previousMatch = true;
							break;

						case "other":
							ReadDB.editIfPreviousMatch(previousMatch, extract);//Edits string to allow new array for next word type
							extract.append(ReadDB.searchForWordEntry(preppedOther, 5, 1, 1, wordAndType[0]));
							previousMatch = true;
							break;
							
						default:
							System.out.println("Unknown type: " + wordAndType[1]);
							unknownWords.add(searchWord);
							continue;
						}
					}
					matchedCount++;
					totalStringSize += extract.length();
					definitions.put(searchWord, extract.toString());
				}
				
				
				long unmatchedCount = wordCount - matchedCount;
				
				StringBuilder allDefinitions = new StringBuilder((int)totalStringSize).append("{ \"words\": [");
				for(String definition : definitions.keySet()) {
					allDefinitions.append(definitions.get(definition)); //Prep for transmission
				}
				
				
				//Add statistics
				String statistics = ",\"statistics\": [{\"matched\" : \"" + matchedCount+ "\"}, {\"unmatched\" : \"" + unmatchedCount + "\"}, {\"total\" : \"" + wordCount + "\"} ]"; 
				allDefinitions.replace(allDefinitions.length() -1, allDefinitions.length(), "]" + statistics + "}");//Corrects ending to allow proper JSON formatting
				//System.out.println(allDefinitions);
				
				Files.deleteIfExists(Paths.get("/home/mkg/Desktop/WordData/allDefinitions.txt"));
				Files.writeString(Paths.get("/home/mkg/Desktop/WordData/allDefinitions.txt"), allDefinitions);
					
				
				boolean showStats = true;
				if(showStats) {
					//Statistics
					System.out.println();
					System.out.println("  Matched Count: " + String.format("%-5s", matchedCount) + " (" + (String.format("%.1f", ((double) matchedCount / wordCount) * 100)) + "%)");
					System.out.println("Unmatched Count: " + String.format("%-5s", unmatchedCount) + " ("	+ (String.format("%.1f", ((double) unmatchedCount / wordCount) * 100)) + "%)");
					System.out.println("     Word Count: " + String.format("%-5s", wordCount));
					
					System.out.println(System.lineSeparator() + "       SQL Time: " + (String.format("%.3f", (double) (System.nanoTime() - sqlStart) / (long) 1000000000)) + " Seconds");
					System.out.println("   Program Time: "+ (String.format("%.3f", (double) (System.nanoTime() - programStart) / (long) 1000000000)) + " Seconds");
				}
				
			} catch (IOException io) {
				System.out.println("IO Exception: " + io.getMessage());
			}

		} catch (SQLException ex) {
			System.out.println("SQL Exception: " + ex.getMessage());
		}
		
	}
	
	private static String searchForWordEntry(PreparedStatement prepped, int columnCount, int rowCount, int argCount, String bareWord) throws SQLException {
		for(int i = 1; i < argCount + 1; i++) {
			prepped.setString(i, bareWord.toLowerCase());
		}

		ResultSet results = prepped.executeQuery();
		
		String wordType = "";
		LinkedHashMap<String, String[]> merger = new LinkedHashMap<String, String[]>();
		int rowsWritten = 0;
		
		boolean matchFound = false;
		int rowIterationCount = 1;
		while(results.next()) {
			matchFound = true;	//Match Confirmed
			wordType = results.getString(4); // Get type
			String[] wordData = new String[2]; //[0] = conjugations/declensions, [1] = translation
			
			String header = results.getString(1) + "," + results.getString(2); //Header will be used to check for duplicate data
			String translation = results.getString(columnCount); //Translations, which will be combined if multiple ones exist
						
			ResultSetMetaData metaData = results.getMetaData();
			
			//TESTING - For debugging to cut down on output
			//columnCount = 2;
			
			StringBuilder dataLine = new StringBuilder();
			for(int i = 1; i < columnCount; i++) {
				String end = (i == columnCount ? "" : ",");
				dataLine.append("\"W" + rowIterationCount + "_" + metaData.getColumnName(i) + "\":\"" + results.getString(i) + "\"" + end); //Build up the word data for this entry
			}
			
			wordData[0] = dataLine.toString(); 
			wordData[1] = translation;
			
			if(!merger.containsKey(header)) { //Add to HashMap in order, preserving orderings for verbs
				merger.put(header, wordData);
				rowsWritten = 0;// Reset as this could be a new word
				rowsWritten++; //Increment as 1 row has now been written
			} else {
				String[] mergedData = merger.get(header);//Get current detail
				
				if(rowsWritten < rowCount) {//If more rows are to be written, otherwise it should be duplicate data
					mergedData[0] = mergedData[0] + wordData[0]; //Merge
					rowsWritten++; 
				}
				
				if(!(mergedData[1].indexOf(wordData[1]) > -1)) {
					mergedData[1] = mergedData[1] + ", " + wordData[1]; //Merge any translations
					merger.put(header, mergedData); //Update
				}
			}
			rowIterationCount++;
		}
		
		
		//Builds a string from captured data for processing in JSON format
		StringBuilder encodedResults = new StringBuilder("{\"" + wordType + "s\": [");
		for(String wordHeader : merger.keySet()) {
			String[] wordData = merger.get(wordHeader);
			encodedResults.append("{");
			String combo = wordData[0] + "\"" + "Translation" + "\":" + "\"" + wordData [1] + "\"";
			encodedResults.append(combo + "},");
		}
		
		encodedResults.replace(encodedResults.length() -1, encodedResults.length(), "]}]},");//Correct the ending when finished
		
		
		return (matchFound == true ? encodedResults.toString() : "null");
	}
	
	private static void editIfPreviousMatch(boolean previousMatch, StringBuilder extract) {
		if(previousMatch) {
			extract.replace(extract.length() -3, extract.length(), ",");
		}
	}
	
	
	
}
