package org.russian.db;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashSet;

import org.russian.statements.CleanerStatements;

public class CleanDB {

	public static void main(String[] args) {
		long programStart = System.nanoTime();
		String oldDatabase = "./Database/Russian_Words_Lite.sqlite3";
		String database = "./Modified Database/Russian_Words_Lite_Modified.sqlite3";
		
		try {
			Files.deleteIfExists(Paths.get(database));
			Files.copy(Paths.get(oldDatabase), Paths.get(database));
		} catch (IOException io) {
			System.out.println("Unable to copy database: " + io.getMessage());
			System.exit(0);
		}
		
		
		boolean inMemory = false;
		String url = (inMemory ? "jdbc:sqlite::memory:" : ("jdbc:sqlite:" + database));

		try (Connection conn = DriverManager.getConnection(url)) {
			try (Statement st = conn.createStatement()) {
				if (inMemory) {
					st.execute("restore from " + database);
				}
				
				System.out.println("Connection to database has been established!");
				long sqlStart = System.nanoTime();
				
				
				PreparedStatement preppedVerbs = conn.prepareStatement(CleanerStatements.extractAllVerbs);
				PreparedStatement preppedNouns = conn.prepareStatement(CleanerStatements.extractAllNouns);
				PreparedStatement preppedAdjectives = conn.prepareStatement(CleanerStatements.extractAllAdjectives);
				PreparedStatement preppedOthers = conn.prepareStatement(CleanerStatements.extractAllOthers);
				StringBuilder variantSQL = new StringBuilder("INSERT INTO variants (bare, type, variant) VALUES" + System.lineSeparator());
				
				
				//Fix идти & пойти, plus one odd partner  
				Statement valueFix = conn.createStatement();
				valueFix.execute("UPDATE verbs SET partner = \"пойти\" WHERE word_id=96"); //Number for partner
				valueFix.execute("UPDATE verbs SET partner = \"идти\" WHERE word_id=164"); //Number for partner
				valueFix.execute("UPDATE verbs SET partner = \"распускать\" WHERE word_id=6273"); //Text in partner
				valueFix.execute("UPDATE verbs SET partner = \"null\" WHERE word_id=43000"); //German partner
				valueFix.execute("UPDATE verbs SET partner = \"null\" WHERE word_id=43386"); //German partner
				valueFix.execute("UPDATE verbs SET partner = \"null\" WHERE word_id=48146"); //German partner
				valueFix.execute("UPDATE verbs SET partner = \"умозаключить\" WHERE word_id=50168"); //Strange lettered partner

				
				//////////////////////////////////////////////////////////////////////// FIX VERBS///////////////////////////////////////////////////////////////////////////////////////
				ResultSet resultsSet = preppedVerbs.executeQuery();
				long count = 0;
				int columnCount = 19;
				String[] row = new String[columnCount];
				StringBuilder updateVerbSQL = new StringBuilder("INSERT INTO verbs (bare, accented, audio, type, aspect, partner, imperative_sg, imperative_pl, "
						+ "past_m, past_f, past_n, past_pl, sg1, sg2, sg3, pl1, pl2, pl3, tl) VALUES" + System.lineSeparator());
				
				System.out.println("============================================");
				System.out.println("Processing Verbs!");
				while (resultsSet.next()) {
					for (int i = 1; i < columnCount + 1; i++) {
						count++;
						String value = resultsSet.getString(i);
						value = (value == null || value.length() == 0 || value.equals("-") || value.equals(".") || value.equals("_") ? "null" : value.replace((char) 39, (char) 769).trim());

						if (!CleanerStatements.nonRussianVerbHeaders.contains(CleanerStatements.verbHeaders[i - 1])) {// If we're working with a value that should be verified
							if (!CleanDB.verifyWord(value)) {
								
								
								if(value.indexOf("//") > -1 || value.indexOf("-;") > -1 || value.indexOf("-") > -1 || value.indexOf("_") > -1 || value.indexOf("._") > -1) {
									value = value.replaceAll("//", ", ");
									value = value.replaceAll("-;", "");
									value = value.replaceAll("-", "");
									value = value.replaceAll("_", "");
									value = value.replaceAll("\\.", "");
								}
								
								if(value.indexOf("$II(") > -1) {//Word ID 52062
									value = value.substring(value.indexOf("$II(") + 4);
								}
							
								//Below loop & IF remove any accent characters and check for verification
								StringBuilder tmpValue  = new StringBuilder(value.length());
								for(char letter : value.toCharArray()) {//Replace any accent characters
									if (!((int) letter == 768)) {//Accent mark 
										if((int) letter == 225) {//Replace á
											tmpValue.append('а');
										} else if((int) letter == 235){//Replace ë
											tmpValue.append('ё');												
										} else {
											tmpValue.append(letter);
										}
									}
								}
								
								if(tmpValue.length() > 0) {
									value = tmpValue.toString();
								}
								
								if (CleanerStatements.verbHeaders[i - 1].equals("partner")) {// If this is a partner value
									if (CleanDB.verifyPartner(value)) {// {Pass if it is values separated by colons
							//			System.out.print(value + " ");
										row[i - 1] = value;
										continue;
									}
								}

								if (!CleanDB.verifyCommaSeparatedWords(value)) {
									System.out.println();
									System.out.print(value);
									System.out.print("-> Dirty " + CleanerStatements.verbHeaders[i - 1] + System.lineSeparator());
									System.out.println("Count: " + count);
									System.exit(0);
								}
							}
						}
						row[i - 1] = value;
					//	 System.out.print(value + " ");
					}
				//	System.out.println();
					CleanDB.appendVariantsRow(row, new int[] {6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17}, variantSQL);
					CleanDB.appendRow(columnCount, row, updateVerbSQL);
					
				}
				
				
				updateVerbSQL.replace(updateVerbSQL.lastIndexOf(","), updateVerbSQL.length(), ";");
//				System.out.println(updateSQL);
				
				
				//System.out.println();
				System.out.println("Verb Count: " + count); 
				////////////////////////////////////////////////////////////////////////END VERBS///////////////////////////////////////////////////////////////////////////////////////
				
				
				
				
				////////////////////////////////////////////////////////////////////////FIX NOUNS///////////////////////////////////////////////////////////////////////////////////////
				resultsSet = preppedNouns.executeQuery();
				count = 0;
				columnCount = 16; 
				row = new String[columnCount];
				StringBuilder updateNounSQL = new StringBuilder("INSERT INTO nouns (bare, accented, audio, type, nom, acc, gen, dat, inst, prep, gender, animate, indeclinable, sg_only, pl_only, tl) VALUES" + System.lineSeparator());
				
				System.out.println("============================================");
				System.out.println("Processing Nouns!");
				while(resultsSet.next()) {
					count++;
					for(int i = 1; i < columnCount + 1; i++) {
						String value = resultsSet.getString(i);
						value = (value == null || value.length() == 0 || value.equals("-") ? "null" : value.replace((char)39, (char)769).trim());
						
						if(!CleanerStatements.nonRussianNounHeaders.contains(CleanerStatements.nounHeaders[i - 1])) {//If we're working with a value that should be verified
							if(!CleanDB.verifyWord(value)) {
								
								if(value.startsWith("*")) {//Remove leading astrix
									value = value.substring(1);
								}
								
								if(value.endsWith("=>")) {//Remove trailing arrow
									value = value.substring(0, value.length() -2);
								}
								
								
								if(value.indexOf("//") > -1) {
									value = value.replaceAll("//", ", ");
								}
								
								
								//Below loop & IF remove any accent characters and check for verification
								StringBuilder tmpValue  = new StringBuilder(value.length());
								for(char letter : value.toCharArray()) {//Replace any accent characters
									if ((int) letter == 1117) {//Avoid ѝ 
										tmpValue.append('и');
									} else if((int) letter == 111){//Avoid o
										tmpValue.append('о');
									} else {
										tmpValue.append(letter);
									}
								}
								
								if(tmpValue.length() > 0) {//If string was modified
									value = tmpValue.toString();
								}
								
								if(CleanDB.verifyCommaSeparatedNounsOrHyphen(value)) {//Allow comma separated values and hyphen
									//System.out.print(value + " ");
									row[i -1] = value;
									continue;
								}
								
								System.out.println();
								System.out.print(value);
								System.out.print("-> Dirty " + CleanerStatements.nounHeaders[i - 1]);
								System.exit(0);
							}
							
							
						}
						//System.out.print(value + " ");
						row[i -1] = value;
					}
					CleanDB.appendVariantsRow(row, new int[] {4, 5, 6, 7, 8, 9}, variantSQL);
					CleanDB.appendRow(columnCount, row, updateNounSQL);
					//System.out.println();
					
				}
				
				updateNounSQL.replace(updateNounSQL.lastIndexOf(","), updateNounSQL.length(), ";");
//				System.out.println(updateSQL);
					
				//System.out.println();
				System.out.println("Noun Count: " + count);   
				////////////////////////////////////////////////////////////////////////END NOUNS///////////////////////////////////////////////////////////////////////////////////////
				
				
				////////////////////////////////////////////////////////////////////////FIX ADJECTIVES///////////////////////////////////////////////////////////////////////////////////////
				resultsSet = preppedAdjectives.executeQuery();
				count = 0;
				columnCount = 17; 
				row = new String[columnCount];
				StringBuilder updateAdjectiveSQL = new StringBuilder("INSERT INTO adjectives (bare, accented, audio, type, comparative, superlative, short_m, short_f, short_n, short_pl, "
						+ "nom, acc, gen, dat, inst, prep, tl) VALUES " + System.lineSeparator());
				
				valueFix.execute("UPDATE adjectives SET short_pl = \"трёхко'мнатны\" WHERE word_id=32930"); //Remove unknown character
				
				System.out.println("============================================");
				System.out.println("Processing Adjectives!");
				while(resultsSet.next()) {
					for(int i = 1; i < columnCount + 1; i++) {
						count++;
						String value = resultsSet.getString(i);
						value = (value == null || value.length() == 0 || value.equals("-") || value.equals("—") ? "null" : value.replace((char)39, (char)769).trim());
						
						if(!CleanerStatements.nonRussianAdjectiveHeaders.contains(CleanerStatements.adjectiveHeaders[i - 1])) {//If we're working with a value that should be verified
							if(!CleanDB.verifyWord(value)) {
								
								if(value.indexOf(";") > -1 || value.indexOf("/ ") > -1 || value.indexOf("(") > -1 
										|| value.indexOf("//") > -1 || value.indexOf("/") > -1 || value.indexOf("[") > -1
											|| value.indexOf(",,") > -1 || value.indexOf("-") > -1 || value.indexOf("f") > -1
											|| value.indexOf("j") > -1 || value.indexOf("s") > -1 || value.indexOf("*") > -1) {//Remove odd characters
									value = value.replaceAll(";", ", ");
									value = value.replaceAll("/ ", ", ");
									value = value.replaceAll("\\(|\\)", "");
									value = value.replaceAll("//", ", ");
									value = value.replaceAll("/", ", ");
									value = value.replaceAll("\\[", "");
									value = value.replaceAll("\\]", "");
									value = value.replaceAll(",,", ", ");
									value = value.replaceAll("-", " ");
									value = value.replaceAll("\\*", "");
									value = value.replaceAll("f", "а");
									value = value.replaceAll("j", "о");
									value = value.replaceAll("s", "ы");
								}
								
								
								
								//Below loop & IF remove any accent characters and check for verification
								StringBuilder tmpValue  = new StringBuilder(value.length());
								for(char letter : value.toCharArray()) {//Replace any accent characters
									if ((int) letter == 116) {//Avoid t 
										tmpValue.append('т');
									} else if ((int) letter == 99) {//Avoid c
										tmpValue.append('с');
									} else if ((int) letter == 97) {//Avoid a
										tmpValue.append('а');
									} else if ((int) letter == 111) {//Avoid o
										tmpValue.append('о');
									} else if ((int) letter == 101) {//Avoid e
										tmpValue.append('е');
									} else if ((int) letter == 96) {//Avoid `
										continue;
									} else if ((int) letter == 1104) {//Avoid ѐ
										tmpValue.append('е');
									} else if ((int) letter == 1117) {//Avoid ѝ
										tmpValue.append('и');
									} else {	
										tmpValue.append(letter);
									}
								}
								
								
								if(tmpValue.length() > 0) {//If string was modified
									value = tmpValue.toString();
								}
								
								if(CleanDB.verifyCommaSeparatedAdjectives(value)) {
									row[i -1] = value;
									//System.out.print(value + " ");
									continue;
								}
								
								System.out.println();
								System.out.print(value);
								System.out.print("-> Dirty " + CleanerStatements.adjectiveHeaders[i - 1]);
								System.exit(0);
							}
						}
						row[i -1] = value;
						//System.out.print(value + " ");
					}
					//System.out.println();
					CleanDB.appendVariantsRow(row, new int[] {4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}, variantSQL);
					CleanDB.appendRow(columnCount, row, updateAdjectiveSQL);
				}
				
				updateAdjectiveSQL.replace(updateAdjectiveSQL.lastIndexOf(","), updateAdjectiveSQL.length(), ";");
//				System.out.println(updateSQL);
				
				//System.out.println();
				System.out.println("Adjective Count: " + count); 
				////////////////////////////////////////////////////////////////////////END ADJECTIVES///////////////////////////////////////////////////////////////////////////////////////
				
				
				////////////////////////////////////////////////////////////////////////FIX OTHERS///////////////////////////////////////////////////////////////////////////////////////
				
				resultsSet = preppedOthers.executeQuery();
				count = 0;
				columnCount = 5;
				StringBuilder updateOtherSQL = new StringBuilder("INSERT INTO others (bare, accented, audio, type, tl) VALUES " + System.lineSeparator());
				
				System.out.println("============================================");
				System.out.println("Processing Others!");
				row = new String[columnCount];
				while (resultsSet.next()) {
					for (int i = 1; i < columnCount + 1; i++) {
						count++;
						String value = resultsSet.getString(i);
						value = (value == null || value.length() == 0 || value.equals("-") || value.equals("—") ? "null": value.replace((char) 39, (char) 769).trim());
	
						if (!CleanerStatements.nonRussianOtherHeaders.contains(CleanerStatements.otherHeaders[i - 1])) {// If we're working with a value that should be verified
							if (!CleanDB.verifyHyphenedOther(value)) {
								
								System.out.println();
								System.out.print(value);
								System.out.print("-> Dirty " + CleanerStatements.otherHeaders[i - 1]);
								System.exit(0);
							}
						}
						row[i -1] = value;
					}
					CleanDB.appendVariantsRow(row, new int[] {0}, variantSQL);
					CleanDB.appendRow(columnCount, row, updateOtherSQL);
				}
				
				updateOtherSQL.replace(updateOtherSQL.lastIndexOf(","), updateOtherSQL.length(), ";");
//				System.out.println(updateSQL);
				
				
				//System.out.println();
				System.out.println("Other Count: " + count);
				////////////////////////////////////////////////////////////////////////END OTHERS///////////////////////////////////////////////////////////////////////////////////////
				
				
				
				
				variantSQL.replace(variantSQL.lastIndexOf(","), variantSQL.length(), ";");
				//System.out.println(variantSQL);
				
				
				
				////////////////////////////////////////////////////////////////////////START SQL CHANGES///////////////////////////////////////////////////////////////////////////////////////
				
				System.out.println("============================================");
				System.out.println("Dropping Tables!");
				
				Statement databaseModifier = conn.createStatement();
				//Drop tables
				databaseModifier.execute("DROP TABLE adjectives");
				databaseModifier.execute("DROP TABLE categories_words2");
				databaseModifier.execute("DROP TABLE conjugations");
				databaseModifier.execute("DROP TABLE declensions");
				databaseModifier.execute("DROP TABLE expressions_words");
				databaseModifier.execute("DROP TABLE nouns");
				databaseModifier.execute("DROP TABLE sentences");
				databaseModifier.execute("DROP TABLE sentences_words");
				databaseModifier.execute("DROP TABLE translations");
				databaseModifier.execute("DROP TABLE verbs");
				databaseModifier.execute("DROP TABLE words");
				databaseModifier.execute("DROP TABLE words_rels");
				
				
				System.out.println("============================================");
				System.out.println("Creating Tables!");
				
				//Add new tables
				databaseModifier.execute("CREATE TABLE verbs ("
					+	"bare TEXT, "
					+	"accented TEXT, " 
					+	"audio TEXT, " 
					+	"type TEXT, "
					+	"aspect TEXT, " 
					+	"partner TEXT, "
					+	"imperative_sg TEXT, " 
					+	"imperative_pl TEXT, " 
					+	"past_m TEXT, " 
					+	"past_f TEXT, " 
					+	"past_n TEXT, "
					+	"past_pl TEXT, " 
					+	"sg1 TEXT, " 
					+	"sg2 TEXT, " 
					+	"sg3 TEXT, " 
					+	"pl1 TEXT, " 
					+	"pl2 TEXT, " 
					+	"pl3 TEXT, " 
					+	"tl TEXT "
					+ ")"
					);
				
				databaseModifier.execute("CREATE TABLE nouns ("
				 + "bare TEXT, "
				 + "accented TEXT, "
				 + "audio TEXT, "
				 + "type TEXT, "
				 + "nom TEXT, "
				 + "acc TEXT, " 
				 + "gen TEXT, "
				 + "dat TEXT, "
				 + "inst TEXT, "
				 + "prep TEXT, "
				 + "gender TEXT, "
				 + "animate TEXT, "
				 + "indeclinable TEXT, "
				 + "sg_only TEXT, "
				 + "pl_only TEXT, "
				 + "tl TEXT "
				 + ")"
				 );
				
				
				databaseModifier.execute("CREATE TABLE adjectives ("
				+ "bare TEXT, "
				+ "accented TEXT, "
				+ "audio TEXT, "
				+ "type TEXT, "
				+ "comparative TEXT, "
				+ "superlative TEXT, "
				+ "short_m TEXT, "
				+ "short_f TEXT, "
				+ "short_n TEXT, "
				+ "short_pl TEXT, "
				+ "nom TEXT, "
				+ "acc TEXT, "
				+ "gen TEXT, "
				+ "dat TEXT, "
				+ "inst TEXT, "
				+ "prep TEXT, "
				+ "tl TEXT "
				+ ")"
				);
				
				databaseModifier.execute("CREATE TABLE others ("
				+ "bare TEXT, "
				+ "accented TEXT, "
				+ "audio TEXT, "
				+ "type TEXT, "
				+ "tl TEXT "
				+ ")"
				);
				
				databaseModifier.execute("CREATE TABLE variants ("
				+ "bare TEXT, "
				+ "type TEXT, "
				+ "variant TEXT "
				+ ")"
				);
						
				System.out.println("============================================");
				System.out.println("Inserting Data!");
				
				databaseModifier.execute(updateVerbSQL.toString());
				System.out.println("Verbs complete!");
				
				databaseModifier.execute(updateNounSQL.toString());
				System.out.println("Nouns complete!");
				
				databaseModifier.execute(updateAdjectiveSQL.toString());
				System.out.println("Adjectives complete!");
				
				databaseModifier.execute(updateOtherSQL.toString());
				System.out.println("Others complete!");
				
				databaseModifier.execute(variantSQL.toString());
				System.out.println("Variants complete!");
				
				//Add indexes (variant for variants table, bare for all others)
				System.out.println("============================================");
				System.out.println("Creating Indexes!");
				
				databaseModifier.execute("CREATE INDEX variantsIndex ON variants(variant)");
				System.out.println("Indexes: [1/5]");
				
				databaseModifier.execute("CREATE INDEX verbsIndex ON verbs(bare)");
				System.out.println("Indexes: [2/5]");
				
				databaseModifier.execute("CREATE INDEX nounsIndex ON nouns(bare)");
				System.out.println("Indexes: [3/5]");
				
				databaseModifier.execute("CREATE INDEX adjectivesIndex ON adjectives(bare)");
				System.out.println("Indexes: [4/5]");
				
				databaseModifier.execute("CREATE INDEX othersIndex ON others(bare)");
				System.out.println("Indexes: [5/5]");
				
				
				System.out.println("============================================");
				System.out.println("Complete!");
				
				////////////////////////////////////////////////////////////////////////END SQL CHANGES///////////////////////////////////////////////////////////////////////////////////////
				
				System.out.println();
				System.out.println("    SQL Time: " + (String.format("%.3f", (double)(System.nanoTime() - sqlStart) /  (long)1000000000)) + " Seconds");
				System.out.println("Program Time: " + (String.format("%.3f", (double)(System.nanoTime() - programStart) /  (long)1000000000)) + " Seconds");
				
			}

		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	
	private static boolean verifyWord(String value) {
		return value.matches("^[А-Яа-яёЁ'́́́]+|null$");
	}
	
	private static boolean verifyCommaSeparatedWords(String value) {
		return value.matches("^[А-Яа-яёЁ'́́́, ]+|null$");
	}
	
	private static boolean verifyPartner(String value) {
		return value.matches("^[А-Яа-яёЁ'́́́; ]+|null$");
	}
	
	private static boolean verifyCommaSeparatedNounsOrHyphen(String value) {
		return value.matches("^[А-Яа-яёЁ'́́́, -]+$");
	}
	
	private static boolean verifyCommaSeparatedAdjectives(String value) {
		return value.matches("^[А-Яа-яёЁ'́́́, ]+$");
	}
	
	private static boolean verifyHyphenedOther(String value) {
		return value.matches("^[А-Яа-яёЁ'́́́-]+$");
	}
	
	private static void appendRow(int columnCount, String[] row, StringBuilder sqlQuery) {
		sqlQuery.append("(");
		for (int i = 0; i < columnCount; i++) {
			String cell = row[i].toLowerCase().replaceAll("\"", "'");
			String endingChar = ((i + 1) == row.length) ? ")," : ",";
			sqlQuery.append("\"" + cell + "\"" + endingChar);
		}
		sqlQuery.append(System.lineSeparator());

		Arrays.fill(row, "");
	}
	
	private static void appendVariantsRow(String[] row, int[] valuesToAdd, StringBuilder variantQuery) {
		for(int variantIndex : valuesToAdd) {
			String word = CleanDB.removeAccent(row[variantIndex].toLowerCase());
			
			if(word.equals("null")) {
				continue;
			}
			
			if(word.indexOf(",") > -1) {
				String[] words = word.split(",");
				for(String splitWord: words) {
					
					String dataToAdd = "(\"" + row[0] + "\", \"" + row[3] + "\", \"" + splitWord.trim() + "\"),";
					
					if(duplicateChecker.contains(dataToAdd)) {
						continue;
					}
					
					duplicateChecker.add(dataToAdd);
					variantQuery.append(dataToAdd + System.lineSeparator());
				}
				continue;
			}
			String dataToAdd = "(\"" + row[0] + "\", \"" + row[3] + "\", \"" + word.toString().trim() + "\"),";
			
			if(duplicateChecker.contains(dataToAdd)) {
				continue;
			}
			
			duplicateChecker.add(dataToAdd);
			variantQuery.append(dataToAdd + System.lineSeparator());
		}
		
		String dataToAdd = "(\"" + row[0] + "\", \"" + row[3] + "\", \"" + row[0] + "\"),";
		if(!duplicateChecker.contains(dataToAdd)) {
			duplicateChecker.add(dataToAdd);
			variantQuery.append(dataToAdd + System.lineSeparator());
		}
		
	}
	
	private static String removeAccent(String word) {
		StringBuilder newWord = new StringBuilder(word.length() - 1);
		for(char letter : word.toCharArray()) {
			if((int)letter != 769) {
				newWord.append(letter);
			}
		}
		return newWord.toString();
	}
	
	
	
	private static HashSet<String> duplicateChecker = new HashSet<>(); //Prevents duplicates when creating variants SQL statement
	
	

}
