package org.russian.db;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class WordExtractor {
	public static void main(String[] args)  throws IOException {
		
		
		
		String inputFile = "/home/mkg/Desktop/RuSubs.srt";
		String outputFile = "/home/mkg/Desktop/RuSubs_Modified.srt";
		
		
		List<String> fileLines = Files.readAllLines(Paths.get(inputFile));
		//List<String> fileLines = Arrays.asList(new String[] { "00:00:24,290 --> 00:00:28,292", "Камеры. Звук. Мотор!. Звук Мотор."});
		
		//Match words with hyphen and normal words
		Pattern pattern = Pattern.compile("([А-Яа-яЁё-]+[А-Яа-яЁё]+)|([А-Яа-яЁё]+)", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE); 
		
		for (int i = 0; i < fileLines.size(); i++) {
			String line = fileLines.get(i);
			StringBuilder newLine = new StringBuilder();
			Matcher matcher = pattern.matcher(line);
			int indexOfPreviousMatch = 0;
			while(matcher.find()) {
				String match = matcher.group();
				int startIndex = matcher.start();
				int endIndex = matcher.end();
				String wordEnding = (endIndex == line.length() ? "" : new String(line.substring(endIndex, endIndex + 1)));
				
				
				System.out.println(match);
				newLine.append(line.substring(indexOfPreviousMatch, startIndex));
				newLine.append("!" + match + "!" + wordEnding);
				indexOfPreviousMatch = endIndex + 1;
			}
			
			
			if(indexOfPreviousMatch < line.length()) {
				newLine.append(line.substring(indexOfPreviousMatch));
			}
			
			fileLines.set(i, newLine.toString());
			
			
		}
		
		if(!Files.exists(Paths.get(outputFile))) {
			Files.createFile(Paths.get(outputFile));
		}
		Files.write(Paths.get(outputFile), fileLines, StandardOpenOption.TRUNCATE_EXISTING);
		
		System.out.println();
		for(String line : fileLines) {
			System.out.println(line);
		}
		
		
		
							
		
		
		
		
		
		
		
		
		
		
		

	}
}
