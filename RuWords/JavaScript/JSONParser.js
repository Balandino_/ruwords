function myFunction() {
	// Declare variables
	var input, filter, ul, li, a, i, txtValue;
	input = document.getElementById('myInput');
	filter = input.value.toLowerCase();
	ul = document.getElementById("myUL");
	li = ul.getElementsByTagName('li');

	// Loop through all list items, and hide those who don't match the search query
	for (i = 0; i < li.length; i++) {
		txtValue = li[i].children[0].id



		if (txtValue.toLowerCase().indexOf(filter) > -1) {
			li[i].style.display = "";
		} else {
			li[i].style.display = "none";
		}
	}
}

function displayTable(event, id) {

	var eventTarget = false;
	if (event.target.className === "header"
		|| event.target.className === "wordEntry"
		|| event.target.parentElement.className === "wordEntry") {

		eventTarget = true;
	}

	//Detects it the tabs' container has been selected
	if (eventTarget) {

		/////Locks so that only 1 word entry can be displayed at any one time
		var displayOnlyOne = true;

		var searchBlock = document.getElementById(id).children[1];
		//var displayStyle = searchBlock.style.display; //Not needed?


		//	Keep list elements coloured after selection
		var header = searchBlock.parentElement.children[0];
		var selectedContainer = document.getElementById(searchBlock.parentElement.id);


		//Displays|hides the tabs when their container is selected
		if (searchBlock.style.display === "") {
			searchBlock.style.display = "block";


			if (displayOnlyOne) {
				//Clear colours on all words before setting one
				var allHeaderwords = document.querySelectorAll(".headerWord");
				var allWordEntrys = document.querySelectorAll(".wordEntry");

				for (var i = 0; i < allHeaderwords.length; i++) {
					allHeaderwords[i].style.backgroundColor = "";
					allWordEntrys[i].style.backgroundColor = "";
				}
			}



			//	Keep list elements coloured after selection
			header.style.backgroundColor = "#C3073F";
			selectedContainer.style.backgroundColor = "#C3073F";


		} else {
			//	Clear element colouring after selection

			searchBlock.style.display = "";

			header.style.backgroundColor = "";
			selectedContainer.style.backgroundColor = "";
		}



		if (displayOnlyOne) {
			var entries = document.getElementsByClassName("wordEntry");
			for (var i = 0; i < entries.length; i++) {
				var entryId = entries[i].id;

				if (!(entryId === searchBlock.parentElement.id)) {
					entries[i].children[1].style.display = "";
				}
			}
		}
	}
}

//Open|Close selected tab data
function openTab(evt, cityName, element) {

	var i, tabContent, tablinks;
	tabContent = element.parentElement.parentElement
		.getElementsByClassName("tabContent");

	for (i = 0; i < tabContent.length; i++) {
		tabContent[i].style.display = "none";
	}

	tablinks = element.parentElement.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}

	document.getElementById(cityName).style.display = "block";
	evt.currentTarget.className += " active";
}

var data = '{ "words": [{"замок": [{"nouns": [{"W1_bare":"замок","W1_accented":"замо́к","W1_audio":"null","W1_type":"noun","W1_nom":"замо́к","W1_acc":"замо́к","W1_gen":"замка́","W1_dat":"замку́","W1_inst":"замко́м","W1_prep":"замке́","W1_gender":"m","W1_animate":"0","W1_indeclinable":"0","W1_sg_only":"0","W1_pl_only":"0","W2_bare":"замок","W2_accented":"замо́к","W2_audio":"null","W2_type":"noun","W2_nom":"замки́","W2_acc":"замки́","W2_gen":"замко́в","W2_dat":"замка́м","W2_inst":"замка́ми","W2_prep":"замка́х","W2_gender":"m","W2_animate":"0","W2_indeclinable":"0","W2_sg_only":"0","W2_pl_only":"0","W3_bare":"замок","W3_accented":"замо́к","W3_audio":"null","W3_type":"noun","W3_nom":"замо́к","W3_acc":"замо́к","W3_gen":"замка́","W3_dat":"замку́","W3_inst":"замко́м","W3_prep":"замке́","W3_gender":"m","W3_animate":"0","W3_indeclinable":"0","W3_sg_only":"0","W3_pl_only":"0","W4_bare":"замок","W4_accented":"замо́к","W4_audio":"null","W4_type":"noun","W4_nom":"замки́","W4_acc":"замки́","W4_gen":"замко́в","W4_dat":"замка́м","W4_inst":"замка́ми","W4_prep":"замка́х","W4_gender":"m","W4_animate":"0","W4_indeclinable":"0","W4_sg_only":"0","W4_pl_only":"0","Translation":"lock, keystone, clasp, fastener"},{"W9_bare":"замок","W9_accented":"за́мок","W9_audio":"null","W9_type":"noun","W9_nom":"за́мок","W9_acc":"за́мок","W9_gen":"за́мка","W9_dat":"за́мку","W9_inst":"за́мком","W9_prep":"за́мке","W9_gender":"null","W9_animate":"null","W9_indeclinable":"0","W9_sg_only":"0","W9_pl_only":"0","W10_bare":"замок","W10_accented":"за́мок","W10_audio":"null","W10_type":"noun","W10_nom":"за́мки","W10_acc":"за́мки","W10_gen":"за́мков","W10_dat":"за́мкам","W10_inst":"за́мками","W10_prep":"за́мках","W10_gender":"null","W10_animate":"null","W10_indeclinable":"0","W10_sg_only":"0","W10_pl_only":"0","W11_bare":"замок","W11_accented":"за́мок","W11_audio":"null","W11_type":"noun","W11_nom":"за́мок","W11_acc":"за́мок","W11_gen":"за́мка","W11_dat":"за́мку","W11_inst":"за́мком","W11_prep":"за́мке","W11_gender":"null","W11_animate":"null","W11_indeclinable":"0","W11_sg_only":"0","W11_pl_only":"0","W12_bare":"замок","W12_accented":"за́мок","W12_audio":"null","W12_type":"noun","W12_nom":"за́мки","W12_acc":"за́мки","W12_gen":"за́мков","W12_dat":"за́мкам","W12_inst":"за́мками","W12_prep":"за́мках","W12_gender":"null","W12_animate":"null","W12_indeclinable":"0","W12_sg_only":"0","W12_pl_only":"0","Translation":"castle"}]}]},{"красный": [{"nouns": [{"W1_bare":"красный","W1_accented":"кра́сный","W1_audio":"https://openrussian.org/audio-shtooka/красный.mp3","W1_type":"noun","W1_nom":"кра́сный","W1_acc":"кра́сного","W1_gen":"кра́сного","W1_dat":"кра́сному","W1_inst":"кра́сным","W1_prep":"кра́сном","W1_gender":"m","W1_animate":"1","W1_indeclinable":"0","W1_sg_only":"0","W1_pl_only":"0","W2_bare":"красный","W2_accented":"кра́сный","W2_audio":"https://openrussian.org/audio-shtooka/красный.mp3","W2_type":"noun","W2_nom":"кра́сные","W2_acc":"кра́сных","W2_gen":"кра́сных","W2_dat":"кра́сным","W2_inst":"кра́сными","W2_prep":"кра́сных","W2_gender":"m","W2_animate":"1","W2_indeclinable":"0","W2_sg_only":"0","W2_pl_only":"0","Translation":"red, communist"}]},{"adjectives": [{"W1_bare":"красный","W1_accented":"кра́сный","W1_audio":"https://openrussian.org/audio-shtooka/красный.mp3","W1_type":"adjective","W1_comparative":"красне́е","W1_superlative":"красне́йший","W1_short_m":"кра́сен","W1_short_f":"красна́","W1_short_n":"кра́сно,красно́","W1_short_pl":"кра́сны,красны́","W1_nom":"кра́сный","W1_acc":"кра́сный,кра́сного","W1_gen":"кра́сного","W1_dat":"кра́сному","W1_inst":"кра́сным","W1_prep":"кра́сном","W2_bare":"красный","W2_accented":"кра́сный","W2_audio":"https://openrussian.org/audio-shtooka/красный.mp3","W2_type":"adjective","W2_comparative":"красне́е","W2_superlative":"красне́йший","W2_short_m":"кра́сен","W2_short_f":"красна́","W2_short_n":"кра́сно,красно́","W2_short_pl":"кра́сны,красны́","W2_nom":"кра́сная","W2_acc":"кра́сную","W2_gen":"кра́сной","W2_dat":"кра́сной","W2_inst":"кра́сной,кра́сною","W2_prep":"кра́сной","W3_bare":"красный","W3_accented":"кра́сный","W3_audio":"https://openrussian.org/audio-shtooka/красный.mp3","W3_type":"adjective","W3_comparative":"красне́е","W3_superlative":"красне́йший","W3_short_m":"кра́сен","W3_short_f":"красна́","W3_short_n":"кра́сно,красно́","W3_short_pl":"кра́сны,красны́","W3_nom":"кра́сное","W3_acc":"кра́сное","W3_gen":"кра́сного","W3_dat":"кра́сному","W3_inst":"кра́сным","W3_prep":"кра́сном","W4_bare":"красный","W4_accented":"кра́сный","W4_audio":"https://openrussian.org/audio-shtooka/красный.mp3","W4_type":"adjective","W4_comparative":"красне́е","W4_superlative":"красне́йший","W4_short_m":"кра́сен","W4_short_f":"красна́","W4_short_n":"кра́сно,красно́","W4_short_pl":"кра́сны,красны́","W4_nom":"кра́сные","W4_acc":"кра́сные,кра́сных","W4_gen":"кра́сных","W4_dat":"кра́сным","W4_inst":"кра́сными","W4_prep":"кра́сных","Translation":"red"}]}]},{"знать": [{"verbs": [{"W1_bare":"знать","W1_accented":"знать","W1_audio":"https://openrussian.org/audio-shtooka/знать.mp3","W1_type":"verb","W1_aspect":"imperfective","W1_partner":"узнать","W1_imperative_sg":"зна́й","W1_imperative_pl":"зна́йте","W1_past_m":"зна́л","W1_past_f":"зна́ла","W1_past_n":"зна́ло","W1_past_pl":"зна́ли","W1_sg1":"зна́ю","W1_sg2":"зна́ешь","W1_sg3":"зна́ет","W1_pl1":"зна́ем","W1_pl2":"зна́ете","W1_pl3":"зна́ют","Translation":"know, be aware, be acquainted, have a knowledge, aristocracy, nobility, the elite, evidently, it seems"},{"W2_bare":"узнать","W2_accented":"узна́ть","W2_audio":"https://openrussian.org/audio-shtooka/узнать.mp3","W2_type":"verb","W2_aspect":"perfective","W2_partner":"знать;узнавать","W2_imperative_sg":"узна́й","W2_imperative_pl":"узна́йте","W2_past_m":"узна́л","W2_past_f":"узна́ла","W2_past_n":"узна́ло","W2_past_pl":"узна́ли","W2_sg1":"узна́ю","W2_sg2":"узна́ешь","W2_sg3":"узна́ет","W2_pl1":"узна́ем","W2_pl2":"узна́ете","W2_pl3":"узна́ют","Translation":"know, recognize, discover, learn, find out, get to know"}]},{"nouns": [{"W1_bare":"знать","W1_accented":"знать","W1_audio":"https://openrussian.org/audio-shtooka/знать.mp3","W1_type":"noun","W1_nom":"null","W1_acc":"null","W1_gen":"null","W1_dat":"null","W1_inst":"null","W1_prep":"null","W1_gender":"f","W1_animate":"0","W1_indeclinable":"0","W1_sg_only":"1","W1_pl_only":"0","Translation":"nobility, noble people"}]}]},{"весь": [{"verbs": [{"W1_bare":"весить","W1_accented":"ве́сить","W1_audio":"null","W1_type":"verb","W1_aspect":"imperfective","W1_partner":"null","W1_imperative_sg":"ве́сь","W1_imperative_pl":"ве́сьте","W1_past_m":"ве́сил","W1_past_f":"ве́сила","W1_past_n":"ве́сило","W1_past_pl":"ве́сили","W1_sg1":"ве́шу","W1_sg2":"ве́сишь","W1_sg3":"ве́сит","W1_pl1":"ве́сим","W1_pl2":"ве́сите","W1_pl3":"ве́сят","Translation":"weigh"}]},{"nouns": [{"W1_bare":"весь","W1_accented":"весь","W1_audio":"https://openrussian.org/audio-shtooka/весь.mp3","W1_type":"noun","W1_nom":"ве́сь","W1_acc":"ве́сь","W1_gen":"ве́си","W1_dat":"ве́си","W1_inst":"ве́сью","W1_prep":"ве́си","W1_gender":"f","W1_animate":"0","W1_indeclinable":"0","W1_sg_only":"0","W1_pl_only":"0","W2_bare":"весь","W2_accented":"весь","W2_audio":"https://openrussian.org/audio-shtooka/весь.mp3","W2_type":"noun","W2_nom":"ве́си","W2_acc":"ве́си","W2_gen":"весе́й","W2_dat":"веся́м","W2_inst":"веся́ми","W2_prep":"веся́х","W2_gender":"f","W2_animate":"0","W2_indeclinable":"0","W2_sg_only":"0","W2_pl_only":"0","W3_bare":"весь","W3_accented":"весь","W3_audio":"https://openrussian.org/audio-shtooka/весь.mp3","W3_type":"noun","W3_nom":"ве́сь","W3_acc":"ве́сь","W3_gen":"ве́си","W3_dat":"ве́си","W3_inst":"ве́сью","W3_prep":"ве́си","W3_gender":"f","W3_animate":"0","W3_indeclinable":"0","W3_sg_only":"0","W3_pl_only":"0","W4_bare":"весь","W4_accented":"весь","W4_audio":"https://openrussian.org/audio-shtooka/весь.mp3","W4_type":"noun","W4_nom":"ве́си","W4_acc":"ве́си","W4_gen":"весе́й","W4_dat":"веся́м","W4_inst":"веся́ми","W4_prep":"веся́х","W4_gender":"f","W4_animate":"0","W4_indeclinable":"0","W4_sg_only":"0","W4_pl_only":"0","Translation":"village, everything"}]},{"adjectives": [{"W1_bare":"весь","W1_accented":"весь","W1_audio":"https://openrussian.org/audio-shtooka/весь.mp3","W1_type":"adjective","W1_comparative":"null","W1_superlative":"null","W1_short_m":"null","W1_short_f":"null","W1_short_n":"null","W1_short_pl":"null","W1_nom":"ве́сь","W1_acc":"ве́сь, всего́","W1_gen":"всего́","W1_dat":"всему́","W1_inst":"все́м","W1_prep":"всём","W2_bare":"весь","W2_accented":"весь","W2_audio":"https://openrussian.org/audio-shtooka/весь.mp3","W2_type":"adjective","W2_comparative":"null","W2_superlative":"null","W2_short_m":"null","W2_short_f":"null","W2_short_n":"null","W2_short_pl":"null","W2_nom":"вся́","W2_acc":"всю́","W2_gen":"все́й","W2_dat":"все́й","W2_inst":"все́й, все́ю","W2_prep":"все́й","W3_bare":"весь","W3_accented":"весь","W3_audio":"https://openrussian.org/audio-shtooka/весь.mp3","W3_type":"adjective","W3_comparative":"null","W3_superlative":"null","W3_short_m":"null","W3_short_f":"null","W3_short_n":"null","W3_short_pl":"null","W3_nom":"всё","W3_acc":"всё","W3_gen":"всего́","W3_dat":"всему́","W3_inst":"все́м","W3_prep":"всём","W4_bare":"весь","W4_accented":"весь","W4_audio":"https://openrussian.org/audio-shtooka/весь.mp3","W4_type":"adjective","W4_comparative":"null","W4_superlative":"null","W4_short_m":"null","W4_short_f":"null","W4_short_n":"null","W4_short_pl":"null","W4_nom":"все́","W4_acc":"все́, все́х","W4_gen":"все́х","W4_dat":"все́м","W4_inst":"все́ми","W4_prep":"все́х","Translation":"all, the whole of, everything, everybody, everyone, no more left"}]}]},{"время": [{"nouns": [{"W1_bare":"время","W1_accented":"вре́мя","W1_audio":"https://openrussian.org/audio-shtooka/время.mp3","W1_type":"noun","W1_nom":"вре́мя","W1_acc":"вре́мя","W1_gen":"вре́мени","W1_dat":"вре́мени","W1_inst":"вре́менем","W1_prep":"вре́мени","W1_gender":"n","W1_animate":"0","W1_indeclinable":"0","W1_sg_only":"0","W1_pl_only":"0","W2_bare":"время","W2_accented":"вре́мя","W2_audio":"https://openrussian.org/audio-shtooka/время.mp3","W2_type":"noun","W2_nom":"времена́","W2_acc":"времена́","W2_gen":"времён","W2_dat":"времена́м","W2_inst":"времена́ми","W2_prep":"времена́х","W2_gender":"n","W2_animate":"0","W2_indeclinable":"0","W2_sg_only":"0","W2_pl_only":"0","Translation":"time, times, tense"}]}]},{"это": [{"adjectives": [{"W1_bare":"этот","W1_accented":"э́тот","W1_audio":"https://openrussian.org/audio-shtooka/этот.mp3","W1_type":"adjective","W1_comparative":"null","W1_superlative":"null","W1_short_m":"null","W1_short_f":"null","W1_short_n":"null","W1_short_pl":"null","W1_nom":"э́тот","W1_acc":"э́тот, э́того","W1_gen":"э́того","W1_dat":"э́тому","W1_inst":"э́тим","W1_prep":"э́том","W2_bare":"этот","W2_accented":"э́тот","W2_audio":"https://openrussian.org/audio-shtooka/этот.mp3","W2_type":"adjective","W2_comparative":"null","W2_superlative":"null","W2_short_m":"null","W2_short_f":"null","W2_short_n":"null","W2_short_pl":"null","W2_nom":"э́та","W2_acc":"э́ту","W2_gen":"э́той","W2_dat":"э́той","W2_inst":"э́той, э́тою","W2_prep":"э́той","W3_bare":"этот","W3_accented":"э́тот","W3_audio":"https://openrussian.org/audio-shtooka/этот.mp3","W3_type":"adjective","W3_comparative":"null","W3_superlative":"null","W3_short_m":"null","W3_short_f":"null","W3_short_n":"null","W3_short_pl":"null","W3_nom":"э́то","W3_acc":"э́то","W3_gen":"э́того","W3_dat":"э́тому","W3_inst":"э́тим","W3_prep":"э́том","W4_bare":"этот","W4_accented":"э́тот","W4_audio":"https://openrussian.org/audio-shtooka/этот.mp3","W4_type":"adjective","W4_comparative":"null","W4_superlative":"null","W4_short_m":"null","W4_short_f":"null","W4_short_n":"null","W4_short_pl":"null","W4_nom":"э́ти","W4_acc":"э́ти, э́тих","W4_gen":"э́тих","W4_dat":"э́тим","W4_inst":"э́тими","W4_prep":"э́тих","Translation":"this"}]},{"others": [{"W1_bare":"это","W1_accented":"э́то","W1_audio":"https://openrussian.org/audio-shtooka/это.mp3","W1_type":"other","Translation":"this is, that is, it"}]}]},{"палить": [{"verbs": [{"W1_bare":"палить","W1_accented":"пали́ть","W1_audio":"null","W1_type":"verb","W1_aspect":"imperfective","W1_partner":"спалить;выпалить;пальнуть;запалить;пальнуть;опалить","W1_imperative_sg":"пали́","W1_imperative_pl":"пали́те","W1_past_m":"пали́л","W1_past_f":"пали́ла","W1_past_n":"пали́ло","W1_past_pl":"пали́ли","W1_sg1":"палю́","W1_sg2":"пали́шь","W1_sg3":"пали́т","W1_pl1":"пали́м","W1_pl2":"пали́те","W1_pl3":"паля́т","Translation":"burn, scorch, fire"},{"W2_bare":"выпалить","W2_accented":"вы́палить","W2_audio":"null","W2_type":"verb","W2_aspect":"perfective","W2_partner":"палить,выпаливать","W2_imperative_sg":"вы́пали","W2_imperative_pl":"вы́палите","W2_past_m":"вы́палил","W2_past_f":"вы́палила","W2_past_n":"вы́палило","W2_past_pl":"вы́палили","W2_sg1":"вы́палю","W2_sg2":"вы́палишь","W2_sg3":"вы́палит","W2_pl1":"вы́палим","W2_pl2":"вы́палите","W2_pl3":"вы́палят","Translation":"shoot, fire, blurt out"},{"W3_bare":"запалить","W3_accented":"запали́ть","W3_audio":"null","W3_type":"verb","W3_aspect":"perfective","W3_partner":"палить,запаливать","W3_imperative_sg":"запали́","W3_imperative_pl":"запали́те","W3_past_m":"запали́л","W3_past_f":"запали́ла","W3_past_n":"запали́ло","W3_past_pl":"запали́ли","W3_sg1":"запалю́","W3_sg2":"запали́шь","W3_sg3":"запали́т","W3_pl1":"запали́м","W3_pl2":"запали́те","W3_pl3":"запаля́т","Translation":"set on fire, set fire, set alight"},{"W4_bare":"опалить","W4_accented":"опали́ть","W4_audio":"null","W4_type":"verb","W4_aspect":"perfective","W4_partner":"палить,опалять,опаливать","W4_imperative_sg":"опали́","W4_imperative_pl":"опали́те","W4_past_m":"опали́л","W4_past_f":"опали́ла","W4_past_n":"опали́ло","W4_past_pl":"опали́ли","W4_sg1":"опалю́","W4_sg2":"опали́шь","W4_sg3":"опали́т","W4_pl1":"опали́м","W4_pl2":"опали́те","W4_pl3":"опаля́т","Translation":"singe"},{"W5_bare":"пальнуть","W5_accented":"пальну́ть","W5_audio":"null","W5_type":"verb","W5_aspect":"perfective","W5_partner":"палить","W5_imperative_sg":"пальни́","W5_imperative_pl":"пальни́те","W5_past_m":"пальну́л","W5_past_f":"пальну́ла","W5_past_n":"пальну́ло","W5_past_pl":"пальну́ли","W5_sg1":"пальну́","W5_sg2":"пальнёшь","W5_sg3":"пальнёт","W5_pl1":"пальнём","W5_pl2":"пальнёте","W5_pl3":"пальну́т","Translation":"to burn"},{"W6_bare":"спалить","W6_accented":"спали́ть","W6_audio":"null","W6_type":"verb","W6_aspect":"perfective","W6_partner":"палить","W6_imperative_sg":"спали́","W6_imperative_pl":"спали́те","W6_past_m":"спали́л","W6_past_f":"спали́ла","W6_past_n":"спали́ло","W6_past_pl":"спали́ли","W6_sg1":"спалю́","W6_sg2":"спали́шь","W6_sg3":"спали́т","W6_pl1":"спали́м","W6_pl2":"спали́те","W6_pl3":"спаля́т","Translation":"incinerate, burn, burn down, reduce to ashes, scorch, singe"}]}]}],"statistics": [{"matched" : "7"}, {"unmatched" : "0"}, {"total" : "7"} ]}';

var shortData = '{"words":[{"знать":[{"verbs":[{"bare": "знать", "translation" : "to know"},{"bare": "узнать", "translation" : "to find out"}]},{"nouns":[{"bare": "замок", "translation" : "castle"}]}]},{"время":[{"nouns" :[{"bare": "время", "translation" : "time"}]}]}]}'

var noun = '{ "words": [{"время": [{"nouns": [{"R1_bare":"время","R1_accented":"вре́мя","R1_audio":"https://openrussian.org/audio-shtooka/время.mp3","R1_type":"noun","R1_nom":"вре́мя","R1_acc":"вре́мя","R1_gen":"вре́мени","R1_dat":"вре́мени","R1_inst":"вре́менем","R1_prep":"вре́мени","R1_gender":"n","R1_animate":"0","R1_indeclinable":"0","R1_sg_only":"0","R1_pl_only":"0","R2_bare":"время","R2_accented":"вре́мя","R2_audio":"https://openrussian.org/audio-shtooka/время.mp3","R2_type":"noun","R2_nom":"времена́","R2_acc":"времена́","R2_gen":"времён","R2_dat":"времена́м","R2_inst":"времена́ми","R2_prep":"времена́х","R2_gender":"n","R2_animate":"0","R2_indeclinable":"0","R2_sg_only":"0","R2_pl_only":"0","Translation":"time, times, tense"}]}]}],"statistics": [{"matched" : "1"}, {"unmatched" : "0"}, {"total" : "1"} ]}';

var toKnow = '{ "words": [{"знать": [{"verbs": [{"W1_bare":"знать","W1_accented":"знать","W1_audio":"https://openrussian.org/audio-shtooka/знать.mp3","W1_type":"verb","W1_aspect":"imperfective","W1_partner":"узнать","W1_imperative_sg":"зна́й","W1_imperative_pl":"зна́йте","W1_past_m":"зна́л","W1_past_f":"зна́ла","W1_past_n":"зна́ло","W1_past_pl":"зна́ли","W1_sg1":"зна́ю","W1_sg2":"зна́ешь","W1_sg3":"зна́ет","W1_pl1":"зна́ем","W1_pl2":"зна́ете","W1_pl3":"зна́ют","Translation":"know, be aware, be acquainted, have a knowledge, aristocracy, nobility, the elite, evidently, it seems"},{"W2_bare":"узнать","W2_accented":"узна́ть","W2_audio":"https://openrussian.org/audio-shtooka/узнать.mp3","W2_type":"verb","W2_aspect":"perfective","W2_partner":"знать;узнавать","W2_imperative_sg":"узна́й","W2_imperative_pl":"узна́йте","W2_past_m":"узна́л","W2_past_f":"узна́ла","W2_past_n":"узна́ло","W2_past_pl":"узна́ли","W2_sg1":"узна́ю","W2_sg2":"узна́ешь","W2_sg3":"узна́ет","W2_pl1":"узна́ем","W2_pl2":"узна́ете","W2_pl3":"узна́ют","Translation":"know, recognize, discover, learn, find out, get to know"}]},{"nouns": [{"W1_bare":"знать","W1_accented":"знать","W1_audio":"https://openrussian.org/audio-shtooka/знать.mp3","W1_type":"noun","W1_nom":"null","W1_acc":"null","W1_gen":"null","W1_dat":"null","W1_inst":"null","W1_prep":"null","W1_gender":"f","W1_animate":"0","W1_indeclinable":"0","W1_sg_only":"1","W1_pl_only":"0","Translation":"nobility, noble people"}]}]}],"statistics": [{"matched" : "1"}, {"unmatched" : "0"}, {"total" : "1"} ]}';

var parsedData = JSON.parse(data);

var words = parsedData["words"];

var wordList = document.createElement("ul");
wordList.setAttribute("id", "myUL");



for (searchWord in words) {//Get array of words
	var newListEntry = document.createElement("li");

	for (bareWord in words[searchWord]) {//All data associated with a word
		//		document.writeln("Word: " + bareWord + "<br />");

		var newWordEntry = document.createElement("div");
		newWordEntry.setAttribute("class", "wordEntry");
		newWordEntry.setAttribute("id", bareWord);
		newWordEntry.setAttribute("onclick", "displayTable(event, this.id)");
		newListEntry.appendChild(newWordEntry);

		var newHeaderWord = document.createElement("div");
		newHeaderWord.setAttribute("class", "headerWord disable-select");
		newHeaderWord.innerText = bareWord;
		newWordEntry.appendChild(newHeaderWord);

		var newWordFormsData = document.createElement("div");
		newWordFormsData.setAttribute("class", "wordFormsData");
		newWordEntry.appendChild(newWordFormsData);

		var newTab = document.createElement("div");
		newTab.setAttribute("class", "tab");
		newWordFormsData.appendChild(newTab);


		var wordCount = 1;
		for (type in words[searchWord][bareWord]) {//Get array of word types (e.g nouns, verbs)
			for (typeEntry in words[searchWord][bareWord][type]) {//Get each type
				var tabCount = 0;
				var verbCount = 0;
				for (entry in words[searchWord][bareWord][type][typeEntry]) {//Each word definition within the type

					var newButton = document.createElement("button");
					if (wordCount === 1) {
						newButton.setAttribute("class", "tablinks active");
					} else {
						newButton.setAttribute("class", "tablinks");
					}

					newButton.setAttribute("onclick", "openTab(event, '" + bareWord + "/" + wordCount + "', this)");
					tabCount++;
					newTab.appendChild(newButton);


					var newTabContent = document.createElement("div");
					newTabContent.setAttribute("id", bareWord + "/" + wordCount);
					newTabContent.setAttribute("class", "tabContent");

					var newTable = document.createElement("table");

					var definition = words[searchWord][bareWord][type][typeEntry][entry];

					var newbuttonTextSet = false;

					var jsonKeys = Object.keys(definition);

					switch (typeEntry) {
						case "nouns":

							var rowStartingNum = jsonKeys[0].substring(1, jsonKeys[0].indexOf("_"));
							var tabHeading = definition["W" + rowStartingNum + "_accented"];
							newButton.innerText = tabHeading;

							var tableTitle = document.createElement("h3");
							tableTitle.innerHTML = "<u>Noun</u><br />" + tabHeading;
							newTabContent.appendChild(tableTitle);

							var mainSeparatorHeadings = ["Singular", "Plural", "Notes", "Translation"];
							var declensionsHeadings = ["Nominative", "Accusative", "Genitive", "Dative", "Instrumental", "Prepositional"];
							var declensionsValues = ["nom", "acc", "gen", "dat", "inst", "prep"];
							var notesHeadings = ["Animate", "Indeclinable", "Gender", "Singular Only", "Plural only"];
							var notesValues = ["animate", "indeclinable", "gender", "sg_only", "pl_only"];

							var firstDeclensionsRow = document.createElement("tr");
							var secondDeclensionsRow = document.createElement("tr");
							var firstDeclensionHeadingsRow = document.createElement("tr");
							var secondDeclensionHeadingsRow = document.createElement("tr");

							var notesHeadingsRow = document.createElement("tr");
							var notesRow = document.createElement("tr");

							var cell;

							for (var i = 0; i < declensionsHeadings.length; i++) {
								cell = document.createElement("td");
								setText(cell, definition["W" + rowStartingNum + "_" + declensionsValues[i]], 1, false)
								firstDeclensionsRow.appendChild(cell);

								cell = document.createElement("td");
								setText(cell, definition["W" + (+rowStartingNum + 1) + "_" + declensionsValues[i]], 1, false)
								secondDeclensionsRow.appendChild(cell);

								cell = document.createElement("th");
								setText(cell, declensionsHeadings[i], 1, false);
								firstDeclensionHeadingsRow.appendChild(cell);

								cell = document.createElement("th");
								setText(cell, declensionsHeadings[i], 1, false);
								secondDeclensionHeadingsRow.appendChild(cell);

								//Notes will have 1 less heading, so skip the final iteration from here
								if (i === declensionsHeadings.length - 1) {
									continue;
								}

								//Gender needs to span 2.  i will equal 2 when on the gender heading
								var colspan = (i === 2 ? 2 : 1);

								cell = document.createElement("th");
								setText(cell, notesHeadings[i], colspan, false);
								notesHeadingsRow.appendChild(cell);

								cell = document.createElement("td");
								setText(cell, definition["W" + rowStartingNum + "_" + notesValues[i]], colspan, false);
								notesRow.appendChild(cell);
							}

							var singularOnly = definition["W" + rowStartingNum + "_" + "sg_only"];
							var pluralOnly = definition["W" + rowStartingNum + "_" + "pl_only"];

							var newRow = document.createElement("tr");
							if (pluralOnly == 0) {
								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[0]));//Singular
								appender(newTable, newRow, firstDeclensionHeadingsRow, firstDeclensionsRow);
							}

							if (singularOnly == 0) {
								newRow = document.createElement("tr");
								newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[1]));//Plural
								appender(newTable, newRow, secondDeclensionHeadingsRow, secondDeclensionsRow);
							}

							newRow = document.createElement("tr");
							newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[2]));//Notes
							appender(newTable, newRow, notesHeadingsRow, notesRow);

							newRow = document.createElement("tr");
							newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[3]));//Translation
							newTable.appendChild(newRow);
							newRow = document.createElement("tr");
							cell = document.createElement("td");
							setText(cell, definition["Translation"], 6);

							newRow.appendChild(cell);
							newTable.appendChild(newRow);

							newTabContent.appendChild(newTable);
							newWordFormsData.appendChild(newTabContent);

							break;

						case "verbs":
							verbCount++;
							
							var rowStartingNum = jsonKeys[0].substring(1, jsonKeys[0].indexOf("_"));
							var tabHeading = definition["W" + rowStartingNum + "_accented"];
							var tabHeadingPrefix = (verbCount === 1 ? "" : "Partner ");
							newButton.textContent = tabHeading;

							var tableTitle = document.createElement("h3");
							tableTitle.innerHTML = "<u>" + tabHeadingPrefix + "Verb</u><br />" + tabHeading;
							newTabContent.appendChild(tableTitle);


							var mainSeparatorHeadings = ["Present", "Past", "Imperative", "Notes", "Translation"];
							var presentHeadings = ["1st Person Singular | Я",
								"2nd Person Singular (Informal) | Ты",
								"2nd Person Plural (Formal) | Вы",
								"3rd Person Singular | Он/Она/Оно",
								"1st Person Plural | Мы",
								"3rd Person Plural | Они"];

							var pastHeadings = ["", "Masculine Singular | Я/Он/Ты", "Feminine Singular  | Я/Она/Ты", "Neuter | Оно", "Plural | Они/Мы/Вы", ""]
							var imperativeHeadings = ["", "", "Singular", "Plural", "", ""]
							var presentValues = ["sg1", "sg2", "pl2", "sg3", "pl1", "pl3"];
							var pastValues = ["", "past_m", "past_f", "past_n", "past_pl", ""];
							var imperativeValues = ["", "", "imperative_sg", "imperative_pl", "", ""];
							var notesHeadings = ["", "", "Aspect", "Partner(s)", "", ""];
							var notesValues = ["", "", "aspect", "partner", "", ""];

							var presentHeadingsRow = document.createElement("tr");
							var pastHeadingsRow = document.createElement("tr");
							var imperativeHeadingsRow = document.createElement("tr");
							var notesHeadingsRow = document.createElement("tr");

							var presentValuesRow = document.createElement("tr");
							var pastValuesRow = document.createElement("tr");
							var imperativeValuesRow = document.createElement("tr");
							var notesValuesRow = document.createElement("tr");

							var cell;
							for (var i = 0; i < presentHeadings.length; i++) {
								cell = document.createElement("th");
								setText(cell, presentHeadings[i], 1, false);
								presentHeadingsRow.appendChild(cell);

								cell = document.createElement("td");
								setText(cell, definition["W" + rowStartingNum + "_" + presentValues[i]], 1, false);
								presentValuesRow.appendChild(cell);

								cell = document.createElement("th");
								setText(cell, pastHeadings[i], 1, false);
								pastHeadingsRow.appendChild(cell);

								cell = document.createElement("td");
								setText(cell, definition["W" + rowStartingNum + "_" + pastValues[i]], 1, false);
								pastValuesRow.appendChild(cell);

								cell = document.createElement("th");
								setText(cell, imperativeHeadings[i], 1, false);
								imperativeHeadingsRow.appendChild(cell);

								cell = document.createElement("td");
								setText(cell, definition["W" + rowStartingNum + "_" + imperativeValues[i]], 1, false);
								imperativeValuesRow.appendChild(cell);

								//Routine that changed the heading Partner(s) to either Partner or Partners, depending on which is appropriate 
								cell = document.createElement("td");
								if(i === 3){
									var partnersValues = definition["W" + rowStartingNum + "_" + notesValues[i]];
									var multiplePartners = (partnersValues.indexOf(";") > -1 || partnersValues.indexOf(",") > -1 ? true : false);
								
									if(multiplePartners){
										setText(cell, "Partners", 1, false);		
									} else {
										setText(cell, "Partner", 1, false);
									}
								} else {
									cell = document.createElement("td");
									setText(cell, notesHeadings[i], 1, false);
								}
								notesHeadingsRow.appendChild(cell);


								notesHeadingsRow.appendChild(cell);
								cell = document.createElement("td");
								setText(cell, definition["W" + rowStartingNum + "_" + notesValues[i]], 1, false);
								notesValuesRow.appendChild(cell);
							}


							var newRow = document.createElement("tr");

							newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[0]));
							appender(newTable, newRow, presentHeadingsRow, presentValuesRow);
							newRow = document.createElement("tr");

							newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[1]));
							appender(newTable, newRow, pastHeadingsRow, pastValuesRow);
							newRow = document.createElement("tr");

							newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[2]));
							appender(newTable, newRow, imperativeHeadingsRow, imperativeValuesRow);
							newRow = document.createElement("tr");

							newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[3]));
							appender(newTable, newRow, notesHeadingsRow, notesValuesRow);
							newRow = document.createElement("tr");

							newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[4]));
							cell = document.createElement("td");
							setText(cell, definition["Translation"], 6);
							appender(newTable, newRow, cell)

							newTabContent.appendChild(newTable);
							newWordFormsData.appendChild(newTabContent);

							break;


						case "adjectives":
							
							var rowStartingNum = jsonKeys[0].substring(1, jsonKeys[0].indexOf("_"));
							var tabHeading = definition["W" + rowStartingNum + "_accented"];
							newButton.innerText = tabHeading;

							var tableTitle = document.createElement("h3");
							tableTitle.innerHTML = "<u>Adjective</u><br />" + tabHeading;
							newTabContent.appendChild(tableTitle);
							
							var mainSeparatorHeadings = ["Masculine", "Feminine", "Neuter", "Plural", "Comparative, Superlative & Short", "Translation"];
							var mainHeadings = ["Nominative",	"Accusative (Animate)",	"Genitive", "Dative", "Instrumental", "Prepositional"];
							var shortHeadings = ["Comparative", "Superlative", "Short Masculine",	"Short Feminine",	"Short Neuter",	"Short Plural"];
							
							var dataValues = ["nom", "acc", "gen", "dat", "inst", "prep"];
							var shortValues = ["comparative", "superlative", "short_m", "short_f", "short_n", "short_pl"];
							
							var masculineHeadingRow = document.createElement("tr");
							var feminineHeadingRow = document.createElement("tr");
							var neuterHeadingRow = document.createElement("tr");
							var pluralHeadingRow = document.createElement("tr");
							var shortHeadingRow = document.createElement("tr");
							
							var masculineValuesRow = document.createElement("tr");
							var feminineValuesRow = document.createElement("tr");
							var neuterValuesRow = document.createElement("tr");
							var pluralValuesRow = document.createElement("tr");
							var shortValuesRow = document.createElement("tr");
													
							var cell;
							for(var i = 0; i < mainHeadings.length; i++){
								
								cell = document.createElement("th");
								setText(cell, mainHeadings[i], 1, false);
								masculineHeadingRow.appendChild(cell);

								cell = document.createElement("td");
								setText(cell, definition["W" + rowStartingNum + "_" + dataValues[i]], 1, true);
								masculineValuesRow.appendChild(cell);
								
								cell = document.createElement("th");
								setText(cell, (i === 1 ? "Accusative" : mainHeadings[i]), 1, false);//Ternary prevents animate heading being used
								feminineHeadingRow.appendChild(cell);
								
								cell = document.createElement("td");
								setText(cell, definition["W" + (+rowStartingNum +1) + "_" + dataValues[i]], 1, false);
								feminineValuesRow.appendChild(cell);
								
								cell = document.createElement("th");
								setText(cell, (i === 1 ? "Accusative" : mainHeadings[i]), 1, false);//Ternary prevents animate heading being used
								neuterHeadingRow.appendChild(cell);
								
								cell = document.createElement("td");
								setText(cell, definition["W" + (+rowStartingNum +2) + "_" + dataValues[i]], 1, false);
								neuterValuesRow.appendChild(cell);
								
								cell = document.createElement("th");
								setText(cell, mainHeadings[i], 1, false);
								pluralHeadingRow.appendChild(cell);
								
								cell = document.createElement("td");
								setText(cell, definition["W" + (+rowStartingNum +3) + "_" + dataValues[i]], 1, true);
								pluralValuesRow.appendChild(cell);
								
								cell = document.createElement("th");
								setText(cell, shortHeadings[i], 1, false);
								shortHeadingRow.appendChild(cell);
								
								cell = document.createElement("td");
								setText(cell, definition["W" + (rowStartingNum) + "_" + shortValues[i]], 1, false);
								shortValuesRow.appendChild(cell);
							
							}
							
							var newRow = document.createElement("tr");
							
							newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[0]));
							appender(newTable, newRow, masculineHeadingRow, masculineValuesRow);
							newRow = document.createElement("tr");	
							
							newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[1]));
							appender(newTable, newRow, feminineHeadingRow, feminineValuesRow);
							newRow = document.createElement("tr");	
							
							newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[2]));
							appender(newTable, newRow, neuterHeadingRow, neuterValuesRow);
							newRow = document.createElement("tr");	
							
							newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[3]));
							appender(newTable, newRow, pluralHeadingRow, pluralValuesRow);
							newRow = document.createElement("tr");	
							
							newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[4]));
							appender(newTable, newRow, shortHeadingRow, shortValuesRow);
							newRow = document.createElement("tr");
							
							newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", mainSeparatorHeadings[5]));
							cell = document.createElement("td");
							setText(cell, definition["Translation"], 6)
							appender(newTable, newRow, cell);
							
							
							
							newTabContent.appendChild(newTable);
							newWordFormsData.appendChild(newTabContent);

							break;


						case "others":

							var rowStartingNum = jsonKeys[0].substring(1, jsonKeys[0].indexOf("_"));
							var tabHeading = definition["W" + rowStartingNum + "_accented"];
							newButton.innerText = tabHeading;

							var tableTitle = document.createElement("h3");
							tableTitle.innerHTML = "<u>Other</u><br />" + tabHeading;
							newTabContent.appendChild(tableTitle);
							
							
							var newRow = document.createElement("tr");
							var cell = document.createElement("td");
							
							newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", "Word"));
							setText(cell, tabHeading, 6);
							appender(newTable, newRow, cell);
							
							
							newRow = document.createElement("tr");
							newRow.appendChild(getNewTableHeadingRow(6, "tableSeparator", "Translation"));
							
							cell = document.createElement("td");
							setText(cell, definition["Translation"], 6);
							appender(newTable, newRow, cell);
							
							


							newTabContent.appendChild(newTable);
							newWordFormsData.appendChild(newTabContent);

							break;

						default:
							var newHeadingsTableRow = document.createElement("tr");

							jsonKeys.forEach(function(key) {
								var newTableCell = document.createElement("td");
								newTableCell.textContent = definition[key];
								newHeadingsTableRow.appendChild(newTableCell);


								if (newbuttonTextSet === false) {
									if (key.endsWith("accented")) {
										newbuttonTextSet = true;
										newButton.innerText = definition[key];
									}
								}
							})

							newTable.appendChild(newHeadingsTableRow);
							newTabContent.appendChild(newTable);
							newWordFormsData.appendChild(newTabContent);
					}

					if (wordCount === 1) {
						newTabContent.style.display = "block";
					}

					wordCount++;
				}
			}
		}
	}
	wordList.appendChild(newListEntry);
}

function getNewTableHeadingRow(columns, classAttribute, text) {
	var newRow = document.createElement("th");
	newRow.setAttribute("colspan", columns);
	newRow.setAttribute("class", classAttribute);
	newRow.textContent = text;

	return newRow;
}

function appender(parent) {
	for (var i = 1; i < arguments.length; i++) {
		parent.appendChild(arguments[i]);
	}
}

function setText(cell, text, colspan, animate) {
	if(text) {//Avoid "" or undefined
				switch (text) {
			case "m":
				text = "Masculine";
				break

			case "f":
				text = "Feminine";
				break

			case "n":
				text = "Neuter";
				break

			case "0":
				text = "No";
				break

			case "1":
				text = "Yes";
				break

			case "null":
				text = "Unknown";
				break;
				
			case "imperfective":
				text = text[0].toUpperCase() + text.substring(1);
				break
				
			case "perfective":
				text = text[0].toUpperCase() + text.substring(1);
				break

			default:
				text = text;
		}
		
		if(animate){
			if(text.indexOf(",") > -1){
				console.log(text);
				text = text.replace(",", " (");
				text = text + ")";
			}
		} else {
			if(text.indexOf(";") > -1 || text.indexOf(",") > -1){
				text = text.replace(/;/g, ",");
				text = text.replace(/,/g, ", ");
			}
		}
	}

	cell.textContent = text;

	if (colspan > 1) {
		cell.setAttribute("colspan", colspan);
	}
}


document.getElementById("wordListContainer").appendChild(wordList);




